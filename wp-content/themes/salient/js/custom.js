document.addEventListener('DOMContentLoaded', () => {

    // Rent Movie

    const movieRent = () => {
        let movieBtn = document.querySelectorAll('.rent-movie')[0];
        let rentMovie = [];
        if (movieBtn) {
            movieBtn.addEventListener("click", () => {
                let localstrg = localStorage.getItem('rent-movie');

                if (localstrg === null) {
                    alert('You have successfully rent the movie');
                    rentMovie.push(movieBtn.id);
                    localStorage.setItem("rent-movie", JSON.stringify(rentMovie));
                } else {

                    if (localstrg.indexOf(movieBtn.id) !== -1) {
                        alert("You have all ready rent this movie!")
                    } else {
                        alert('You have successfully rent the movie');
                        rentMovie.push(movieBtn.id);
                        localStorage.setItem("rent-movie", JSON.stringify(rentMovie));
                    }
                }
            });
        }
    }
    movieRent();

    // Buy Movie

    const movieBuy = () => {
        let byMovieBtn = document.querySelectorAll('.buy-movie')[0];
        let buyMovie = [];
        if (byMovieBtn) {
            byMovieBtn.addEventListener("click", () => {
                let localstrg = localStorage.getItem('buy-movie');
                console.log(localstrg);

                if (localstrg === null) {
                    alert('You have successfully purchase the movie');
                    buyMovie.push(byMovieBtn.id);
                    localStorage.setItem("buy-movie", JSON.stringify(buyMovie));
                } else {

                    if (localstrg.indexOf(byMovieBtn.id) !== -1) {
                        alert("You have all ready bought this movie!")
                    } else {
                        alert('You have successfully purchase the movie');
                        rentMovie.push(byMovieBtn.id);
                        localStorage.setItem("buy-movie", JSON.stringify(buyMovie));
                    }
                }
            });
        }
    }
    movieBuy();

});